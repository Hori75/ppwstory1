from django.shortcuts import render

def index(request):
    return render(request, 'myprofile/index.html')

def viewProfile(request):
    return render(request, 'myprofile/profile.html')
